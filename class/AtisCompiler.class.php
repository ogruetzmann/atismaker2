<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
class AtisCompiler
{

    public function __construct($report)
    {
        $this->_SetFilenames($report);
        $this->report = $report;
        $this->_InitAtisFields();
    }

    private function _GetIcaoRegion($icao)
    {
        $file = FileParser::LoadFile('icao_regions');
        foreach ($file as $value)
        {
            $value = trim($value);
            if (preg_match("%^$value%", $icao))
            {
                return $value;
            }
        }
        return NULL;
    }

    private function _SetFilenames($report)
    {
        $icao                = $report['icao'];
        $region              = $this->_GetIcaoRegion($icao);
        $this->airportConfig = $icao . '.config';
        $this->airportFormat = $icao . '.format';
        $this->airportUser   = $icao . '.user';
        $this->globalConfig  = 'global.config';
        $this->globalFormat  = 'global.format';
        $this->globalUser    = 'global.user';

        if ($region)
        {
            $this->regionalConfig = $region . '.config';
            $this->regionalFormat = $region . '.format';
            $this->regionalUser   = $region . '.user';
        }
        else
        {
            $this->regionalConfig = $this->globalConfig;
            $this->regionalFormat = $this->globalFormat;
            $this->regionalUser   = $this->globalUser;
        }
    }

    private function _InitWeather()
    {
        $file = FileParser::LoadFile('atisfields');
        foreach ($file as $value)
        {
            $this->atis[] = trim($value);
        }
        print_r($this->atis);
    }

    private function _SetValuesFromMetar()
    {
        
    }

}
