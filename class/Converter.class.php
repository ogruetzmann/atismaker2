<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
class Converter
{

    public function ConvertPressure($pressure)
    {
        if ($pressure >= 2000)
        {
            // Return millibars
            return round($pressure * 0.33863);
        }
        else
        {
            // return inHG * 100;
            return round($pressure * 2.9530);
        }
    }

    public function CelsiusToFahrenheit($celsius)
    {
        return round($celsius * 1.8 + 32);
    }

    public function FahrenheitToCelsius($fahrenheit)
    {
        return round(($fahrenheit - 32) * 5 / 9);
    }

    public function KnotsToMps($knots)
    {
        return round($knots * 0.51444444444);
    }

    public function MpsToKnots($mps)
    {
        return round($mps * 1.9438444924574);
    }

}
