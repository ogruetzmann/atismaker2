<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
class MetarPart
{

    public function __construct($string)
    {
        $this->_SetGroups($string);
    }

    private function _SetGroups($string)
    {
        $this->auto            = new GroupAuto($string);
        $this->wind            = new GroupWind($string);
        $this->wind_vrb        = new GroupWindVariability($string);
        $this->visibility      = new GroupVisibility($string);
        $this->visibility_min  = new GroupMinimumVisibility($string);
        $this->rvr             = new GroupRvr($string);
        $this->weather         = new GroupWeather($string);
        $this->clouds          = new GroupClouds($string);
        $this->temperature     = new GroupTemperature($string);
        $this->pressure        = new GroupPressure($string);
        $this->recent          = new GroupRecentWeather($string);
        $this->windshear       = new GroupWindshear($string);
        $this->sea             = new GroupSea($string);
        $this->runwayCondition = new GroupRunwayCondition($string);
        $this->designator      = new GroupTrendIndicator($string);

        $this->report['auto']             = $this->auto->Get();
        $this->report['wind']             = $this->wind->Get();
        $this->report['wind_vrb']         = $this->wind_vrb->Get();
        $this->report['visibility']       = $this->visibility->Get();
        $this->report['visibility_min']   = $this->visibility_min->Get();
        $this->report['rvr']              = $this->rvr->Get();
        $this->report['weather']          = $this->weather->Get();
        $this->report['clouds']           = $this->clouds->Get();
        $this->report['temperature']      = $this->temperature->Get();
        $this->report['pressure']         = $this->pressure->Get();
        $this->report['weather_recent']   = $this->recent->Get();
        $this->report['windshear']        = $this->windshear->Get();
        $this->report['sea']              = $this->sea->Get();
        $this->report['runway_condition'] = $this->runwayCondition->Get();
        $this->report['designator']       = $this->designator->Get();
    }

    public function Get()
    {
        return $this->report;
    }

}
