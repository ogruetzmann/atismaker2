<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */

        const confDir = 'config/';

class Settings
{

    private $configDirectory = confDir;
    private $icaoRegions     = NULL;
    private $globalConfig    = NULL;
    private $globalFormat    = NULL;
    private $globalUser      = NULL;
    private $regionalConfig  = NULL;
    private $regionalFormat  = NULL;
    private $regionalUser    = NULL;
    private $localConfig     = NULL;
    private $localFormat     = NULL;
    private $localUser       = NULL;
    private $config          = NULL;
    private $format          = NULL;
    private $user            = NULL;

    public function __construct($icao, $configDirectory = NULL)
    {
        $this->_SetFilenames($icao, $configDirectory);
        $this->_LoadConfig();
        $this->_LoadFormat();
        $this->_LoadUser();
    }

    public function Output()
    {
        print_r($this->config);
        print_r($this->format);
        print_r($this->user);
    }

    public function GetConfig()
    {
        return $this->config;
    }

    public function GetFormat()
    {
        return $this->format;
    }

    public function GetUser()
    {
        return $this->user;
    }

    private function _SetFilenames($icao, $configDirectory)
    {
        if ($configDirectory)
        {
            $this->configDirectory = $configDirectory;
        }
        $this->icaoRegions = $this->configDirectory . 'icao_regions';
        $region            = $this->_GetIcaoRegion($icao);

        $this->localConfig  = $this->configDirectory . $icao . '.config';
        $this->localFormat  = $this->configDirectory . $icao . '.format';
        $this->localUser    = $this->configDirectory . $icao . '.user';
        $this->globalConfig = $this->configDirectory . 'global.config';
        $this->globalFormat = $this->configDirectory . 'global.format';
        $this->globalUser   = $this->configDirectory . 'global.user';

        if ($region)
        {
            $this->regionalConfig = $this->configDirectory . $region . '.config';
            $this->regionalFormat = $this->configDirectory . $region . '.format';
            $this->regionalUser   = $this->configDirectory . $region . '.user';
        }
    }

    private function _GetIcaoRegion($icao)
    {
        if (!$file = $this->_LoadFile($this->icaoRegions))
        {
            return NULL;
        }
        foreach ($file as $value)
        {
            $value = trim($value);
            if (preg_match("%^$value%", $icao))
            {
                return $value;
            }
        }
        return NULL;
    }

    protected function _LoadFile($filename)
    {
        if ($filename == NULL || !is_file($filename))
        {
            return NULL;
        }
        return file($filename);
    }

    private function _LoadConfig()
    {
        $result       = NULL;
        $this->_GetFromFile($this->globalConfig, $result);
        $this->_GetFromFile($this->regionalConfig, $result);
        $this->_GetFromFile($this->localConfig, $result);
        $this->config = $result;
    }

    private function _LoadFormat()
    {
        $result       = NULL;
        $this->_GetFromFile($this->globalFormat, $result);
        $this->_GetFromFile($this->regionalFormat, $result);
        $this->_GetFromFile($this->localFormat, $result);
        $this->format = $result;
    }

    private function _LoadUser()
    {
        $result     = NULL;
        $this->_GetFromFile($this->globalUser, $result);
        $this->_GetFromFile($this->regionalUser, $result);
        $this->_GetFromFile($this->localUser, $result);
        $this->user = $result;
    }

    private function _GetFromFile($filename, &$data)
    {
        if (!$file = $this->_LoadFile($filename))
        {
            return;
        }
        foreach ($file as $value)
        {
            $result = explode(':', $value);
            if (count($result) == 2)
            {
                $data[trim($result[0])] = trim($result[1]);
            }
            elseif (count($result) == 3)
            {
                $data[trim($result[0])][trim($result[1])] = trim($result[2]);
            }
        }
    }

}
