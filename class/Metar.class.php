<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
include_once './class/MetarPart.class.php';
include_once './class/MetarGroup.class.php';

class Metar
{

    private $metar           = NULL;
    private $present         = NULL;
    private $trend           = NULL;
    private $remarks         = NULL;
    private $present_string  = NULL;
    private $trend_string    = NULL;
    private $remarks_string  = NULL;
    private $time            = NULL;
    private $station         = NULL;
    private $pattern_present = '%(\s(NOSIG|TEMPO|BECMG|RMK).*)%';
    private $pattern_trend   = "%.*((?:NOSIG|TEMPO|BECMG).*?)(?:$|RMK.*)%";
    private $pattern_remarks = '%(RMK.*)%';

    public function __construct($metar)
    {
        $this->metar = $metar;
        $this->_Split();
    }

    public function Get()
    {
        return array(
            'general' => array_merge($this->time, $this->station),
            'present' => $this->present->Get(),
            'trend'   => $this->trend->Get(),
            'remarks' => NULL);
    }

    private function _Split()
    {
        $this->_SetPresent();
        $this->_SetTrend();
        $this->_SetRemarks();
        $this->_SetStation();
        $this->_SetTime();
    }

    private function _SetPresent()
    {
        $result               = preg_split($this->pattern_present, $this->metar, -1, PREG_SPLIT_NO_EMPTY);
        $this->present_string = trim($result[0]);
        $this->present        = new MetarPart($this->present_string);
    }

    private function _SetTrend()
    {
        $res = preg_match($this->pattern_trend, $this->metar, $result);
        if ($res)
        {
            $this->trend_string = trim($result[1]);
            $this->trend        = new MetarPart($this->trend_string);
        }
        else
        {
            $this->trend = new MetarPart('');
        }
    }

    private function _SetRemarks()
    {
        $res = preg_match($this->pattern_remarks, $this->metar, $result);
        if ($res)
        {
            $this->remarks_string = trim($result[1]);
            $this->remarks        = new MetarPart($this->remarks_string);
        }
        else
        {
            
        }
    }

    private function _SetStation()
    {
        $station       = new GroupStation($this->present_string);
        $this->station = $station->Get();
    }

    private function _SetTime()
    {
        $time       = new GroupTime($this->present_string);
        $this->time = $time->Get();
    }

}
