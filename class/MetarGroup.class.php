<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
abstract class MetarGroup
{

    protected $string  = NULL;
    protected $result  = NULL;
    protected $pattern = NULL;

    public function __construct($string)
    {
        $this->string = $string;
        $this->Parse();
    }

    abstract public function Parse();

    public function Get()
    {
        return $this->result;
    }

    public function _ParseWeatherGroups($input)
    {

        $result = NULL;
        foreach ($input as $value)
        {
            if (strlen(trim($value)))
            {
                preg_match_all($this->pattern_single, $value, $result);
                $this->_SetWeatherValues($result[0]);
            }
        }
    }

    public function _SetWeatherValues($input)
    {
        $iterator = &$this->result[];
        foreach ($input as $key => $value)
        {
            if (strlen(trim($value)))
            {
                $iterator[] = $value;
            }
        }
    }

}

class GroupStation extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%^[A-Z]{4}(?:\b)%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result = array('icao' => trim($result[0]));
        }
    }

}

class GroupTime extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\s(\d{2})(\d{4})Z\s%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result = array('day' => $result[1], 'time' => $result[2]);
        }
    }

}

class GroupAuto extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\s(AUTO)\s%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result = array('auto' => 1);
        }
    }

}

class GroupWind extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%(\d{3}|VRB)(\d{2})G?(\d{2})?(KT|MPS)%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $unit                        = strtolower($result[4]);
            $this->result['direction']   = $result[1];
            $this->result["speed_$unit"] = (integer) $result[2];
            if (strlen($result[3]))
            {
                $this->result["maximum_$unit"] = (integer) $result[3];
            }
            $this->_ConvertWinds($result[4]);
        }
    }

    private function _ConvertWinds($unit)
    {
        $converter = new Converter();
        if ($unit == 'KT')
        {
            $this->result['speed_mps'] = $converter->KnotsToMps($this->result['speed_kt']);
            if (array_key_exists('maximum_kt', $this->result))
            {
                $this->result['maximum_mps'] = $converter->KnotsToMps($this->result['maximum_kt']);
            }
        }
        else
        {
            $this->result['speed_kt'] = $converter->MpsToKnots($this->result['speed_mps']);
            if (array_key_exists('maximum_mps', $this->result))
            {
                $this->result['maximum_kt'] = $converter->MpsToKnots($this->result['maximum_mps']);
            }
        }
    }

}

class GroupWindVariability extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%(\d{3})V(\d{3})%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result['vrb1'] = $result[1];
            $this->result['vrb2'] = $result[2];
        }
    }

}

class GroupVisibility extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\b(?:(?:(\d{4})(?:NDV)?)|(CAVOK))\b%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        foreach ($result as $value)
        {
            $this->result['visibility'] = $value;
        }
    }

}

class GroupMinimumVisibility extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\s(?:(\d{4})([NESW\/]{1,2})\b)|\d{4}(NDV)\b%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if (in_array('NDV', $result))
        {
            $this->result['ndv'] = 1;
        }
        elseif ($number)
        {
            $this->result['distance']  = $result[1];
            $this->result['direction'] = $result[2];
        }
    }

}

class GroupRvr extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%R(\d{2}[LRC]?)\/([PM])?(\d{4})V?([PM])?(\d{4})?([UDN]?)\b%";
        $result        = NULL;
        $number        = preg_match_all($this->pattern, $this->string, $result);
        if ($number)
        {
            for ($i = 0; $i < $number; $i++)
            {
                $this->result[$i]['rwy']      = $result[1][$i];
                $this->result[$i]['rvr1_mod'] = $result[2][$i];
                $this->result[$i]['rvr1']     = $result[3][$i];
                $this->result[$i]['rvr2_mod'] = $result[4][$i];
                $this->result[$i]['rvr2']     = $result[5][$i];
                $this->result[$i]['trend']    = $result[6][$i];
            }
        }
    }

}

class GroupWeather extends MetarGroup
{

    public function Parse()
    {
        $this->pattern        = "%\s(?:[\+\-]?)(?:VC)?(?:MI|BC|DR|BL|SH|TS|FZ|PR)?(?:DZ|RA|SN|IC|PL|GR|GS|UP)*(?:BR|FG|FU|VA|DU|SA|HZ)?(?:PO|SQ|FC|SS)?\b%";
        $this->pattern_single = "%(?:[\+\-])?(?:VC)?(?:MI|BC|DR|BL|SH|TS|FZ|PR)?(?:DZ|RA|SN|IC|PL|GR|GS|UP)?(?:BR|FG|FU|VA|DU|SA|HZ)?(?:PO|SQ|FC|SS)?%";
        $result               = NULL;
        $number               = preg_match_all($this->pattern, $this->string, $result);
        foreach ($result as $value)
        {
            $this->_ParseWeatherGroups($value);
        }
    }

}

class GroupClouds extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%(FEW|SCT|BKN|OVC)(\d{3})(TCU|CB|\/\/\/)?%";
        $result        = NULL;
        $number        = preg_match_all($this->pattern, $this->string, $result);
        if ($number)
        {
            for ($i = 0; $i < $number; $i++)
            {
                $this->result[$i]['coverage'] = $result[1][$i];
                $this->result[$i]['height']   = $result[2][$i] * 100;
                if (strlen($result[3][$i]))
                {
                    $this->result[$i]['type'] = $result[3][$i];
                }
            }
        }
    }

}

class GroupTemperature extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\s(M?\d{2})/(M?\d{2})\s%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result['temperature'] = $result[1];
            $this->result['dewpoint']    = $result[2];
        }
    }

}

class GroupPressure extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%(Q|A)(\d{4})%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if (!$number)
        {
            return;
        }
        $converter = new Converter();
        if ($result[1] == 'A')
        {
            $this->result['pressure_inhg'] = (integer) $result[2];
            $this->result['pressure_hpa']  = $converter->ConvertPressure($this->result['pressure_inhg']);
        }
        else
        {
            $this->result['pressure_hpa']  = (integer) $result[2];
            $this->result['pressure_inhg'] = $converter->ConvertPressure($this->result['pressure_hpa']);
        }
    }

}

class GroupRecentWeather extends MetarGroup
{

    public function Parse()
    {
        $this->pattern        = "%\sRE(?:[\+\-]?)(?:VC)?(?:MI|BC|DR|BL|SH|TS|FZ|PR)?(?:DZ|RA|SN|IC|PL|GR|GS|UP)*(?:BR|FG|FU|VA|DU|SA|HZ)?(?:PO|SQ|FC|SS)?\b%";
        $this->pattern_single = "%RE(?:[\+\-])?(?:VC)?(?:MI|BC|DR|BL|SH|TS|FZ|PR)?(?:DZ|RA|SN|IC|PL|GR|GS|UP)?(?:BR|FG|FU|VA|DU|SA|HZ)?(?:PO|SQ|FC|SS)?%";
        $result               = NULL;
        $number               = preg_match_all($this->pattern, $this->string, $result);
        foreach ($result as $value)
        {
            $this->_ParseWeatherGroups($value);
        }
    }

}

class GroupWindshear extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%\sWS (R\d\d[LRC]{0,2}|ALL RWY)%";
        $result        = NULL;
        $number        = preg_match_all($this->pattern, $this->string, $result);
        foreach ($result[1] as $value)
        {
            $this->result['windshear'][] = preg_replace("#^R#", "", $value);
        }
    }

}

class GroupSea extends MetarGroup
{

    public function Parse()
    {
        //TODO GroupSea
    }

}

class GroupRunwayCondition extends MetarGroup
{

    public function Parse()
    {
        //TODO GroupRunwayCondition
    }

}

class GroupTrendIndicator extends MetarGroup
{

    public function Parse()
    {
        $this->pattern = "%(BECMG|TEMPO|NOSIG)%";
        $result        = NULL;
        $number        = preg_match($this->pattern, $this->string, $result);
        if ($number)
        {
            $this->result = $result[0];
        }
    }

}
