<?php

/** This file is part of atismaker2.

  atismaker2 is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  atismaker2 is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with atismaker2.  If not, see <http://www.gnu.org/licenses/>.

  Diese Datei ist Teil von atismaker2.

  atismaker2 ist Freie Software: Sie können es unter den Bedingungen
  der GNU General Public License, wie von der Free Software Foundation,
  Version 3 der Lizenz oder (nach Ihrer Wahl) jeder späteren
  veröffentlichten Version, weiterverbreiten und/oder modifizieren.

  atismaker2 wird in der Hoffnung, dass es nützlich sein wird, aber
  OHNE JEDE GEWÄHELEISTUNG, bereitgestellt; sogar ohne die implizite
  Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
  Siehe die GNU General Public License für weitere Details.

  Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
  Programm erhalten haben. Wenn nicht, siehe <http://www.gnu.org/licenses/>* */
spl_autoload_register(
        function($class) {
    include_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'class' .
            DIRECTORY_SEPARATOR . $class . '.class.php';
});

$valid = array(
    'debug'  => FILTER_VALIDATE_INT,
    'online' => FILTER_UNSAFE_RAW,
    'metar'  => FILTER_UNSAFE_RAW,
    'arrrwy' => FILTER_UNSAFE_RAW,
    'deprwy' => FILTER_UNSAFE_RAW,
    'info'   => FILTER_UNSAFE_RAW);

$input = filter_input_array(INPUT_GET, $valid);

if ($input['online'])
{
    $icao  = strtoupper($input['online']);
    $file  = file("http://weather.noaa.gov/pub/data/observations/metar/stations/$icao.TXT");
    $metar = preg_grep("#$icao.*#", $file)[1];
}

if ($input['metar'])
{
    $metar = $input['metar'];
}

$met    = new Metar($metar);
$report = $met->Get();

$settings = new Settings($report['general']['icao']);
var_dump($settings);

if ($input['debug'] == 1)
{
    phpinfo();
}
